(function() {
    'use strict';
    app.factory('scheduleFactory', scheduleFactory);
    scheduleFactory.$inject = ['$timeout'];

    function scheduleFactory($timeout) {

        var scheduleData = [];
        var recordToSave = [];
        var completeData;
        var truckDetails;
        var eftposBarcodeList = [];

        var service = {
            getScheduleData: getScheduleData,
            setScheduleData: setScheduleData,
            updateDeliveryStatus: updateDeliveryStatus,
            getRecordToSave: getRecordToSave,
            setRecordToSave: setRecordToSave,
            setCompleteData: setCompleteData,
            getCompleteData: getCompleteData,
            getTruckDetails: getTruckDetails,
            setTruckDetails: setTruckDetails,
            getEftposTruckList: getEftposTruckList,
            setEftposTruckList: setEftposTruckList,
            addRecord: addRecord
        };

        return service;

        ///////////


        function getScheduleData() {
            //stateName = "AdminBoard.ViewContract";

            return scheduleData;
        }

        function setScheduleData(data) {
            scheduleData = data;
            return scheduleData;
        }

        function updateDeliveryStatus(data) {
            scheduleData.push(data);

        }

        function getRecordToSave() {
            return recordToSave;
        }

        function setRecordToSave(data) {
            recordToSave = data;
            return recordToSave;
        }

        function getCompleteData() {
            return completeData;
        }

        function setCompleteData(data) {
            completeData = data;
            return completeData;
        }

        function addRecord(data) {
            recordToSave.push(data);
            app.addRecordToSave(data);
        }

        function getTruckDetails() {
            return truckDetails;
        }

        function setTruckDetails(data) {
            truckDetails = data;
            return truckDetails;
        }

        function getEftposTruckList() {
            return eftposBarcodeList;
        }

        function setEftposTruckList(data) {
            eftposBarcodeList = data;
            return eftposBarcodeList;
        }
    }
})();