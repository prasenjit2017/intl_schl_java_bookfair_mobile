app.factory('adapter', ['$http', '$q', '$state','localStorageService',
	function($http, $q, $state,localStorageService) {

		var currentURL = {};
		var params = "bookfair-qa.sintl.org/scholastic/";
		var baseURL = "http://" + params;


		function getServiceData(data,url){
			var def = $q.defer();
			var reqURL = baseURL + url;
			var req = {
				method : 'POST',
				url : reqURL,
				data : data
				//headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			};
			$http(req).success(function(data) {
				def.resolve(data);
			}).error(function() {
				def.reject("Failed to get Localize String");
			});
			return def.promise;
		}

		function getServiceDataGet(data,url){
        			var def = $q.defer();
        			var reqURL = baseURL + url;
        			var req = {
        				method : 'GET',
        				url : reqURL
        				//headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
        			};
        			$http(req).success(function(data) {
        				def.resolve(data);
        			}).error(function() {
        				def.reject("Failed to get Localize String");
        			});
        			return def.promise;
        		}


		return {
        			getServiceData : getServiceData,
        			getServiceDataGet:getServiceDataGet
        		};


	}]);