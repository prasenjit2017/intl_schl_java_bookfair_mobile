var fileSync = {
    saveLoginDetails: function(userName, password, userDetails,successCallback) {
        var userNamePassword = [{
            "userName": userName,
            "password": password,
            "user": userDetails
        }];

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: true
            }, gotDir);
        }

        function gotDir(dirEntry) {
            dirEntry.getFile("login.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {
                writeFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            console.log("error on create file");
        }

        function fail() {
            console.log("error on create directory");
        }

        function writeFile(fileEntry, dataObj) {
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    console.log("success in writing login file");
                    successCallback("Success");
                };

                fileWriter.onerror = function(e) {
                    console.log("Error in writing login file")
                };

                if (!dataObj) {
                    dataObj = new Blob([JSON.stringify(userNamePassword)], {
                        type: 'text/plain'
                    });
                }

                fileWriter.write(dataObj);
            });
        }
    },


    loginOffline: function(userName, password, successCallback, errorCallback) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFileSystem, fail);

        function gotFileSystem(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDirectory);
        }

        function gotDirectory(dirEntry) {

            dirEntry.getFile("login.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                readUserRoleFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function readUserRoleFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {

                    var result = JSON.parse(this.result);
                    var userName1 = result[0].userName;
                    var password1 = result[0].password;

                    if (userName === userName1 && password === password1) {
                        successCallback("success");

                    } else {
                        errorCallback("Please check the credentials");
                    }


                };

                reader.readAsText(file);

            }, onErrorReadFile);


        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }
    },


    fetchUserDetails: function(successCallback, errorCallback) {


        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFileSystem, fail);

        function gotFileSystem(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDirectory);
        }

        function gotDirectory(dirEntry) {

            dirEntry.getFile("login.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                readUserRoleFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function readUserRoleFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {

                    var result = JSON.parse(this.result);
                    var userDetails = result[0].user;
                    successCallback(userDetails);
                };

                reader.readAsText(file);

            }, onErrorReadFile);


        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }



    },

    saveScheduleData: function(data) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);


        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            dirEntry.getFile("scheduleData.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {

                var isAppend = true;

                writeFile(fileEntry, null, isAppend);

            }, onErrorCreateFile);
        }

        function onErrorCreateFile() {
            console.log("Error in creating file")
        }

        function fail() {
            console.log("Failed to load file system");
        }

        function writeFile(fileEntry, dataObj, isAppend) {
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    console.log("successfully saved");
                };
                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                };
                //                    if (!dataObj) {
                //                        dataObj = new Blob([JSON.stringify(data)], { type: 'text/plain' });
                //                    }
                //                    fileWriter.write(dataObj);

                if (isAppend) {
                    try {
                        fileEntry.file(function(file) {
                            var reader = new FileReader();
                            reader.onloadend = function() {
                                //console.log("Successful file read: " + this.result);
                                if (this.result !== null && this.result !== '' && this.result!=="null") {
                                    var saveContractData = JSON.parse(this.result);

                                    if (!dataObj) {
                                        for (var i = 0; i < saveContractData.length; i++) {
                                            if (saveContractData[i].scheduleID === data.scheduleID) {
                                                saveContractData[i] = data;
                                                break;
                                            }

                                        }
                                        //console.log(saveContractData);
                                        dataObj = new Blob([JSON.stringify(saveContractData)], {
                                            type: 'application/json'
                                        });
                                    }
                                }
                                fileWriter.write(dataObj);
                            };
                            reader.readAsText(file);
                        }, onErrorReadFile);
                    } catch (e) {
                        console.log("file doesn't exist!");
                    }
                }
            });
        }

        function onErrorReadFile() {
            console.log("Failed to read File");
        }
    },

    fetchScheduleData: function(license, successCallback, errorCallback) {

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFileSystem, fail);

        function gotFileSystem(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDirectory);
        }

        function gotDirectory(dirEntry) {

            dirEntry.getFile("scheduleData.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                readUserRoleFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function readUserRoleFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {

                    var result = JSON.parse(this.result);
                    var scheduleData = {};

                    var todayDate = new Date();
                    //alert($scope.todayDate.toString());

                    var monthNames = [
                        "January", "February", "March",
                        "April", "May", "June", "July",
                        "August", "September", "October",
                        "November", "December"
                    ];

                    var month = todayDate.getMonth() + 1;
                    if (month < 10) {
                        month = "0" + month;
                    }

                    var tarikh=todayDate.getDate();

					if (tarikh < 10) {
							tarikh = "0" + tarikh;
					}

                     todayDate=todayDate.getFullYear()+"-"+month+"-"+tarikh;
                    //todayDate = todayDate.getDate() + "-" + month + "-" + todayDate.getFullYear();


                    if (result !== null && result !== undefined && result !== "") {
                        for (var i = 0; i < result.length; i++) {
                            //if(result[i].date===todayDate && result[i].truckLicenseNo===license){
                            //		scheduleData=result[i];
                            //		break;

                            //}

                            if (result[i].status !== 'Completed' && result[i].truckLicenseNo === license) {
                                var dateRange = result[i].scheduleRange;
                                if (dateRange.toLowerCase() !== 'na') {
                                    dateRange = dateRange.replace(/\s/g, "");
                                    var range=dateRange.split("to");

                                    var firstDate = new Date(range[0]);
                                    var lastDate = new Date(range[1]);

                                    firstDate=firstDate.setHours(0, 0, 0, 0);
                                    lastDate=lastDate.setHours(0, 0, 0, 0);
                                    var currentDate = new Date();
                                    currentDate=currentDate.setHours(0, 0, 0, 0);

                                    if (firstDate <= currentDate && lastDate >= currentDate) {
                                        scheduleData = result[i];
                                        break;
                                    }

                                } else {
                                    if (result[i].date!==null && result[i].date!==undefined && result[i].date === todayDate) {
                                        scheduleData = result[i];
                                        break;
                                    }

                                }
                                //                            scheduleData = result[i];
                                //                            break;
                            }

                        }
                    }




                    successCallback(scheduleData);
                };

                reader.readAsText(file);

            }, onErrorReadFile);


        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }

    },
    updateScheduleData: function(data) {
        fileSync.saveScheduleData(data);

    },

    updateScheduleData1: function(data, successCallback, errorCallback) {

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);


        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            dirEntry.getFile("scheduleData.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                writeFile(fileEntry, null);

            }, onErrorCreateFile);
        }

        function onErrorCreateFile() {
            console.log("Error in creating file")
        }

        function fail() {
            console.log("Failed to load file system");
        }

        function writeFile(fileEntry, dataObj) {
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    console.log("successfully saved");

                    successCallback("success");

                };

                fileWriter.onerror = function(e) {
                    errorCallback("Failed file write: " + e.toString());
                };
                //                    if (!dataObj) {
                //                        dataObj = new Blob([JSON.stringify(data)], { type: 'text/plain' });
                //                    }
                //                    fileWriter.write(dataObj);


                try {
                    fileEntry.file(function(file) {
                        var reader = new FileReader();
                        reader.onloadend = function() {
                            //console.log("Successful file read: " + this.result);
                            if (this.result !== null && this.result !== '' && this.result!=="null") {
                                var saveContractData = JSON.parse(this.result);
                                //                                    var newContract=data;

                                for (var i = 0; i < saveContractData.length; i++) {
                                    if (saveContractData[i].scheduleID === data.scheduleID) {
                                        saveContractData[i] = data;
                                        break;
                                    }

                                }
                                if (!dataObj) {
                                    //     newContract.mobilerowId=saveContractData.length;
                                    //  saveContractData.push(newContract);
                                    //console.log(saveContractData);
                                    dataObj = new Blob([JSON.stringify(saveContractData)], {
                                        type: 'application/json'
                                    });
                                }
                            }
                            fileWriter.write(dataObj);
                        };
                        reader.readAsText(file);
                    }, onErrorReadFile);
                } catch (e) {
                    console.log("file doesn't exist!");
                }
            });
        }

        function onErrorReadFile() {
            console.log("Failed to read File");
        }

    },

    addRecordToSave: function(data) {

        var options = {
            dimBackground: true
        };

        SpinnerPlugin.activityStart("Saving Data..", options);
        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            dirEntry.getFile("delivery.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {
                var isAppend = true;
                writeFile(fileEntry, null, isAppend);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            console.log("Failed to create file");
            SpinnerPlugin.activityStop();
        }

        function fail() {
            console.log("Failed to load file system");
            SpinnerPlugin.activityStop();
        }


        function writeFile(fileEntry, dataObj, isAppend) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    ////console.log("Successful file write...");

                    SpinnerPlugin.activityStop();

                    // successCallback("200");
                    // readFile(fileEntry);
                };
                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                    SpinnerPlugin.activityStop();
                };
                if (isAppend) {
                    try {
                        fileEntry.file(function(file) {
                            var reader = new FileReader();
                            reader.onloadend = function() {
                                //console.log("Successful file read: " + this.result);
                                if (this.result !== null && this.result !== '') {
                                    var saveContractData = JSON.parse(this.result);
                                    var newContract = data;
                                    if (!dataObj) {
                                        //     newContract.mobilerowId=saveContractData.length;
                                        saveContractData.push(newContract);
                                        //console.log(saveContractData);
                                        dataObj = new Blob([JSON.stringify(saveContractData)], {
                                            type: 'application/json'
                                        });
                                    }
                                } else {
                                    if (!dataObj) {
                                        //requestData[0].mobilerowId=0;
                                        // saveContractData.push(newContract);
                                        dataObj = new Blob([JSON.stringify(requestData)], {
                                            type: 'application/json'
                                        });
                                    }
                                }
                                fileWriter.write(dataObj);
                            };
                            reader.readAsText(file);
                        }, onErrorReadFile);
                    } catch (e) {
                        console.log("file doesn't exist!");
                        SpinnerPlugin.activityStop();
                    }
                }
            });
        }

        function onErrorReadFile() {
            console.log("Failed to read File");
            SpinnerPlugin.activityStop();
        }

    },
    savePushScheduleData: function(data, successCallback, errorCallback) {

        var options = {
            dimBackground: true
        };

        SpinnerPlugin.activityStart("Saving Data..", options);
        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            dirEntry.getFile("scheduleData.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {
                var isAppend = true;
                writeFile(fileEntry, null, isAppend);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
            SpinnerPlugin.activityStop();
        }

        function fail() {
            errorCallback("Failed to load file system");
            SpinnerPlugin.activityStop();
        }


        function writeFile(fileEntry, dataObj, isAppend) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {

                    SpinnerPlugin.activityStop();

                    successCallback("successfully saved");
                };
                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                    SpinnerPlugin.activityStop();
                };
                if (isAppend) {
                    try {
                        fileEntry.file(function(file) {
                            var reader = new FileReader();
                            reader.onloadend = function() {
                                //console.log("Successful file read: " + this.result);
                                if (this.result !== null && this.result !== '' && this.result !== undefined && this.result !== "null") {
                                    var saveContractData = JSON.parse(this.result);
                                    var newContract = data;
                                    if (!dataObj) {
                                        // newContract.mobileRowId=saveContractData.length;
                                        var scheduleIdList = [];
                                        for (var i = 0; i < saveContractData.length; i++) {
                                            scheduleIdList.push(saveContractData[i].scheduleID);
                                        }

                                        if (!scheduleIdList.includes(newContract.scheduleID)) {
                                            newContract.status = "Driver Accepted";
                                            saveContractData.push(newContract);
                                            dataObj = new Blob([JSON.stringify(saveContractData)], {
                                                type: 'application/json'
                                            });
                                            fileWriter.write(dataObj);

                                        } else {
                                            SpinnerPlugin.activityStop();
                                            successCallback("Record Already present");
                                        }

                                    }
                                } else {
                                    if (!dataObj) {
                                        //	requestData[0].mobileRowId=0;
                                        requestData[0].status = "Driver Accepted";
                                        dataObj = new Blob([JSON.stringify(requestData)], {
                                            type: 'application/json'
                                        });
                                        fileWriter.write(dataObj);
                                    }
                                }

                            };
                            reader.readAsText(file);
                        }, onErrorReadFile);
                    } catch (e) {
                        errorCallback("file doesn't exist!");
                        SpinnerPlugin.activityStop();
                    }
                }
            });
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File");
        }

    },

    savePushScheduleDataMultiple: function(data, successCallback, errorCallback) {
        var options = {
            dimBackground: true
        };

        SpinnerPlugin.activityStart("Saving Data..", options);
        var requestData = data;


        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            dirEntry.getFile("scheduleData.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {
                var isAppend = true;
                writeFile(fileEntry, null, isAppend);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
        SpinnerPlugin.activityStop();
            errorCallback("Failed to create file");
        }

        function fail() {
        SpinnerPlugin.activityStop();
            errorCallback("Failed to load file system");
        }


        function writeFile(fileEntry, dataObj, isAppend) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {

                    SpinnerPlugin.activityStop();

                    successCallback("successfully saved");
                };
                fileWriter.onerror = function(e) {
                SpinnerPlugin.activityStop();
                    console.log("Failed file write: " + e.toString());
                };
                if (isAppend) {
                    try {
                        fileEntry.file(function(file) {
                            var reader = new FileReader();
                            reader.onloadend = function() {
                                //console.log("Successful file read: " + this.result);
                                if (this.result !== null && this.result !== '' && this.result !== undefined && this.result !== "null") {
                                    var saveContractData = JSON.parse(this.result);
                                    var newContract = data;
                                    if (!dataObj) {
                                        // newContract.mobileRowId=saveContractData.length;
                                        var scheduleIdList = [];
                                        for (var i = 0; i < saveContractData.length; i++) {
                                            scheduleIdList.push(saveContractData[i].scheduleID);
                                        }


                                        for (var i = 0; i < newContract.length; i++) {
                                            if (!scheduleIdList.includes(newContract[i].scheduleID)) {
                                                newContract[i].status = "Driver Accepted";
                                                saveContractData.push(newContract[i]);

                                                if (i == newContract.length - 1) {
                                                    dataObj = new Blob([JSON.stringify(saveContractData)], {
                                                        type: 'application/json'
                                                    });
                                                    fileWriter.write(dataObj);
                                                }

                                            } else {
                                                if (i == newContract.length - 1) {
                                                    SpinnerPlugin.activityStop();
                                                    successCallback("Successful");
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (!dataObj) {
                                        //	requestData[0].mobileRowId=0;
                                        for (var i = 0; i < requestData.length; i++) {
                                            requestData[i].status = "Driver Accepted";
                                        }
                                        dataObj = new Blob([JSON.stringify(requestData)], {
                                            type: 'application/json'
                                        });
                                        fileWriter.write(dataObj);
                                    }
                                }

                                //                                dataObj = new Blob([JSON.stringify(saveContractData)], {
                                //                                    type: 'application/json'
                                //                                });
                                //                                fileWriter.write(dataObj);

                            };
                            reader.readAsText(file);
                        }, onErrorReadFile);
                    } catch (e) {
                        errorCallback("file doesn't exist!");
                        SpinnerPlugin.activityStop();
                    }
                }
            });
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File");
            SpinnerPlugin.activityStop();
        }



    },

    fetchCountPendingRequest: function(successCallback, errorCallback) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFileSystem, fail);

        function gotFileSystem(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDirectory);
        }

        function gotDirectory(dirEntry) {

            dirEntry.getFile("scheduleData.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                readUserRoleFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function readUserRoleFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {

                    var result = JSON.parse(this.result);
                    var pendingCount = 0;
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].status === 'Finalized') {
                            pendingCount++;

                        }

                    }


                    successCallback(pendingCount);
                };

                reader.readAsText(file);

            }, onErrorReadFile);


        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }

    },

    fetchPendingSchedule: function(successCallback, errorCallback) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFileSystem, fail);

        function gotFileSystem(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDirectory);
        }

        function gotDirectory(dirEntry) {

            dirEntry.getFile("scheduleData.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                readUserRoleFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function readUserRoleFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {

                    var result = JSON.parse(this.result);
                    var scheduleData = [];
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].status === 'Finalized') {
                            scheduleData.push(result[i]);
                        }
                    }


                    successCallback(scheduleData);
                };

                reader.readAsText(file);

            }, onErrorReadFile);


        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }

    },

    fetchAllData: function(successCallback, errorCallback) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFileSystem, fail);
        var options = {
            dimBackground: true
        };

        SpinnerPlugin.activityStart("Checking data to sync", options);

        function gotFileSystem(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDirectory);
        }

        function gotDirectory(dirEntry) {

            dirEntry.getFile("delivery.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                readUserRoleFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function readUserRoleFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {

                    var result = JSON.parse(this.result);

                    SpinnerPlugin.activityStop();
                    successCallback(result);

                };

                reader.readAsText(file);

            }, onErrorReadFile);


        }

        function onErrorCreateFile() {
            SpinnerPlugin.activityStop();
            errorCallback("Failed to create file");
        }

        function fail() {
            SpinnerPlugin.activityStop();
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            SpinnerPlugin.activityStop();
            errorCallback("Failed to read File");
        }
    },
    deleteAllData: function(data) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
        var options = {
            dimBackground: true
        };

        SpinnerPlugin.activityStart("Please wait..", options);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("bookFair", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("delivery.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    for (var i = data.length - 1; i >= 0; i--) {

                        for (var j = 0; j < result.length; j++) {
                            if (result[j].sequenceID === data[i]) {
                                result.splice(j, 1);
                            }
                        }

                    }



                    fileEntry.createWriter(function(fileWriter) {

                        fileWriter.onwriteend = function() {
                            ////console.log("Successful file write...");
                            SpinnerPlugin.activityStop();


                            // readFile(fileEntry);
                        };

                        fileWriter.onerror = function(e) {
                            ////console.log("Failed file write: " + e.toString());
                            SpinnerPlugin.activityStop();
                        };

                        var dataObj = null;

                        // If data object is not passed in,
                        // create a new Blob instead.

                        if (!dataObj) {
                            dataObj = new Blob([JSON.stringify(result)], {
                                type: 'application/json'
                            });
                        }

                        fileWriter.write(dataObj);
                    });

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            console.log("Failed to create file");
            SpinnerPlugin.activityStop();
        }

        function fail() {
            console.log("Failed to load file system");
            SpinnerPlugin.activityStop();
        }

        function onErrorReadFile() {
            console.log("Failed to read File");
            SpinnerPlugin.activityStop();
        }
    }

}