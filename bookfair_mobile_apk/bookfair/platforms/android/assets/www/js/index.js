/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
        app.createDataBase();

    },

    createDataBase: function() {
        //dbFunction.createDataBase();
    },

    saveLoginDetails: function(userName, password, userDetails,successCallback) {
        //alert("userName::: "+userName+" and password::: "+password);
        var success = function(data) {
                    successCallback(data);
                }
        fileSync.saveLoginDetails(userName, password, userDetails,success);
    },

    loginOffline: function(userName, password, successCallback, errorCallback) {
        var success = function(data) {
            successCallback(data);
        }

        var error = function(message) {
            errorCallback(message);
        }

        fileSync.loginOffline(userName, password, success, error);
    },

    fetchUserDetails: function(successCallback, errorCallback) {
        var success = function(data) {
            successCallback(data);
        }

        var error = function(message) {
            errorCallback(message);
        }
        fileSync.fetchUserDetails(success, error);
    },

    saveScheduleData: function(data, successCallback, errorCallback) {

        var success = function(data) {
            successCallback(data);
        }
        var error = function(message) {
            errorCallback(message);
        }
        fileSync.saveScheduleData(data, success, error);
    },

    fetchScheduleData: function(data, successCallback, errorCallback) {
        var success = function(data) {
            successCallback(data);
        }
        var error = function(message) {
            errorCallback(message);
        }

        fileSync.fetchScheduleData(data, success, error);
    },

    updateScheduleData: function(data) {
        fileSync.updateScheduleData(data);
    },

    updateScheduleData1: function(data, successCallback, errorCallback) {

        var success = function(data) {
            successCallback(data);
        }

        var error = function(message) {
            errorCallback(message);
        }
        fileSync.updateScheduleData1(data, success, error);
    },

    addRecordToSave: function(data) {
        fileSync.addRecordToSave(data);
    },
    savePushScheduleData: function(data, successCallback, errorCallback) {


        var success = function(data) {
            successCallback(data);
        }

        var error = function(message) {
            errorCallback(message);
        }
        fileSync.savePushScheduleData(data, success, error);
    },

    savePushScheduleDataMultiple: function(data, successCallback, errorCallback) {
        var success = function(data) {
            successCallback(data);
        }
        var error = function(message) {
            errorCallback(message);
        }
        fileSync.savePushScheduleDataMultiple(data, success, error);

    },
    fetchCountPendingRequest: function(successCallback, errorCallback) {
        var success = function(data) {
            successCallback(data);
        }

        var error = function(message) {
            errorCallback(message);
        }
        fileSync.fetchCountPendingRequest(success, error);

    },
    fetchPendingSchedule: function(successCallback, errorCallback) {
        var success = function(data) {
            successCallback(data);
        }

        var error = function(message) {
            errorCallback(message);
        }
        fileSync.fetchPendingSchedule(success, error);
    },
    fetchAllData: function(successCallback, errorCallback) {
        var success = function(data) {
            successCallback(data);
        }

        var error = function(message) {
            errorCallback(message);
        }
        fileSync.fetchAllData(successCallback, errorCallback);
    },
    deleteAllData: function(data) {
        fileSync.deleteAllData(data);
    }
};

app.initialize();