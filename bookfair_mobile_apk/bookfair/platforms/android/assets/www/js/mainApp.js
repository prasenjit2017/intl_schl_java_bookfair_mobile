var app = angular.module("ScholasticBookfair", ['ui.router', 'ngTouch',
    'ngResource', 'ui.grid', 'ui.grid.cellNav', 'ui.grid.selection', 'LocalStorageModule',
    'ui.grid.autoResize', 'ngAnimate', 'ui.bootstrap', 'ngCookies', 'base64', 'ngCordova'
]);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('Login', {
            url: "/Login",
            views: {

                'header': {
                    templateUrl: "app/modules/header/login-view.html",
                },
                'main': {
                    templateUrl: "app/login/login-view.html",
                },
                'footer': {
                    templateUrl: "app/modules/footer/view.html"
                }
            }
        })

        .state('MobileDashboard', {
            url: "/MobileDashboard",
            views: {
                'header': {
                    templateUrl: "app/modules/header/header.html",
                },
                'main': {
                    templateUrl: "app/dashboard/dashboard-view.html"
                }
                /*,
                				'footer':{
                					templateUrl : "app/modules/footer/view.html"
                				}*/
            }

        })

        .state('Schedule', {
            url: "/Schedule",
            views: {
                'header': {
                    templateUrl: "app/modules/header/header.html",
                },
                'main': {
                    templateUrl: "app/truck-schedule/truck-schedule.html"
                }
                /*,
                					'footer':{
                						templateUrl : "app/modules/footer/view.html"
                					}*/
            }
        })

        .state('Destination', {
            url: "/Destination",
            views: {
                'header': {
                    templateUrl: "app/modules/header/header.html",
                },
                'main': {
                    templateUrl: "app/destination/destination.html"
                }
                /*,
                        					'footer':{
                        						templateUrl : "app/modules/footer/view.html"
                        					}*/
            }
        })

        .state('DeliveryChecklist', {
            url: "/DeliveryChecklist",
            views: {
                'header': {
                    templateUrl: "app/modules/header/header.html",
                },
                'main': {
                    templateUrl: "app/delivery-checklist/delivery-checklist.html"
                }
                /*,
                                					'footer':{
                                						templateUrl : "app/modules/footer/view.html"
                                					}*/
            }
        })

        .state('Support', {
            url: "/Support",
            views: {
                'header': {
                    templateUrl: "app/modules/header/header.html",
                },
                'main': {
                    templateUrl: "app/support.html"
                }
                /*,
                                        					'footer':{
                                        						templateUrl : "app/modules/footer/view.html"
                                        					}*/
            }
        })


        .state('MyProfile', {
            url: "/MyProfile",
            views: {
                'header': {
                    templateUrl: "app/modules/header/header.html",
                },
                'main': {
                    templateUrl: "app/my-profile/my-profile.html"
                },
                'footer': {
                    templateUrl: "app/modules/footer/view.html"
                }
            }
        })

        .state('PendingRequests', {
            url: "/PendingRequests",
            views: {
                'header': {
                    templateUrl: "app/modules/header/header.html",
                },
                'main': {
                    templateUrl: "app/pending-requests/pending-requests.html"
                }
                /*,
                                                        					'footer':{
                                                        						templateUrl : "app/modules/footer/view.html"
                                                        					}*/
            }
        })

        .state('Notification', {
            url: "/Notification",
            views: {
                'header': {
                    templateUrl: "app/modules/header/header.html",
                },
                'main': {
                    templateUrl: "app/notification/notification.html"
                }
                /*,
                                                                					'footer':{
                                                                						templateUrl : "app/modules/footer/view.html"
                                                                					}*/
            }
        })

        .state('MySchedule', {
            url: "/MySchedule",
            views: {
                'header': {
                    templateUrl: "app/modules/header/header.html",
                },
                'main': {
                    templateUrl: "app/my-schedule/my-schedule.html"
                }
                /*,
                                                                        					'footer':{
                                                                        						templateUrl : "app/modules/footer/view.html"
                                                                        					}*/
            }
        })

        .state('Settings', {
            url: "/Settings",
            views: {
                'header': {
                    templateUrl: "app/modules/header/header.html",
                },
                'main': {
                    templateUrl: "app/settings/settings.html"
                }
                /*,
                                                                                					'footer':{
                                                                                						templateUrl : "app/modules/footer/view.html"
                                                                                					}*/
            }
        })

        .state('DestinationComplete', {
            url: "/DestinationComplete",
            views: {
                'header': {
                    templateUrl: "app/modules/header/header.html",
                },
                'main': {
                    templateUrl: "app/destination-complete/destination-complete.html"
                }
                /*,
                                                                                        					'footer':{
                                                                                        						templateUrl : "app/modules/footer/view.html"
                                                                                        					}*/
            }
        })

        .state('ConnectTruck', {
            url: "/ConnectTruck",
            views: {
                'header': {
                    templateUrl: "app/modules/header/login-view.html",
                },
                'main': {
                    templateUrl: "app/connect-truck/connect-truck.html"
                }
                /*,
                                                                                                					'footer':{
                                                                                                						templateUrl : "app/modules/footer/view.html"
                                                                                                					}*/
            }
        })

    $urlRouterProvider.otherwise('Login');


}]);