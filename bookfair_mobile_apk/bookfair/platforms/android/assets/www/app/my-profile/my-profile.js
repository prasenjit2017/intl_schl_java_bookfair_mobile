app.controller("profileController", ['$scope', '$http', '$q', '$state', 'scheduleFactory','localStorageService','adapter', function($scope, $http, $q, $state, scheduleFactory,localStorageService,adapter) {

    $scope.confirmPasswordError = false;
    $scope.passwordError = false;
    $scope.liscensePlate = "";

    $scope.truckDetails = scheduleFactory.getTruckDetails();

    app.fetchUserDetails(successCallback, errorCallback);

    function successCallback(result) {
        $scope.userData = result;
        $scope.$apply();
        //alert($scope.userData);
    }

    function errorCallback(message) {
        alert(message);
    }


    $scope.checkPassword = function() {

        if ($scope.newPassword !== "") {
            if ($scope.newPassword.length >= 6) {
                $('.caution:eq(0)').css("color",
                    "#85CD00");
                console
                    .log("Password length color green");
            } else {
                $('.caution:eq(0)').css("color", "red");
                console
                    .log("Password length color red");
            }

            if (/\d/.test($scope.newPassword)) {
                $('.caution:eq(3)').css("color",
                    "#85CD00");
                console.log("Password has one number");
            } else {
                $('.caution:eq(3)').css("color", "red");
                console
                    .log("Password dont have any number");
            }

            if (/[a-z]/g.test($scope.newPassword)) {
                $('.caution:eq(1)').css("color",
                    "#85CD00");
                console
                    .log("Password has one lower case");
            } else {
                $('.caution:eq(1)').css("color", "red");
                console
                    .log("Password dont have lower case");
            }

            if (/[A-Z]/g.test($scope.newPassword)) {
                $('.caution:eq(2)').css("color",
                    "#85CD00");
                console
                    .log("Password has one upper case");
            } else {
                $('.caution:eq(2)').css("color", "red");
                console
                    .log("Password dont have upper case");
            }

            if (/(?=.*[!@#\$%\^&\*])/g
                .test($scope.newPassword)) {
                $('.caution:eq(4)').css("color",
                    "#85CD00");
                console
                    .log("Password has one special character");
            } else {
                $('.caution:eq(4)').css("color", "red");
                console
                    .log("Password dont have special character");
            }

        } else {
            $('.caution').css("color", "red");
        }
    }

    $scope.confirmPasswordCheck = function() {
        if ($scope.newPassword !== $scope.confirmPassword) {
            $scope.confirmPasswordError = true;
            $scope.passwordError = true;
        } else {
            $scope.confirmPasswordError = false;
            $scope.passwordError = false;
        }
        return $scope.passwordError;
    }

    function checkValidation() {
        var strongRegex = new RegExp(
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})");

        if (strongRegex.test($scope.newPassword) &&
            $scope.newPassword !== $scope.confirmPassword) {
            $scope.passwordError = true;
            $scope.confirmPasswordError = true;
        } else if ($scope.newPassword != null && $scope.newPassword != '' && $scope.newPassword != undefined) {
            $scope.passwordError = true;
        } else {
            $scope.passwordError = false;
        }
        if ($scope.newPassword != null && $scope.newPassword != '' && $scope.newPassword != undefined) {
            $scope.samePasswordError = true;
        } else {
            $scope.samePasswordError = false;
        }

        return $scope.passwordError;
    }


    $scope.savePassword = function() {
        if (checkValidation() && !$scope.confirmPasswordCheck()) {
            alert("OK");
        }
    }

    $scope.changeTruck = function() {

        var reqData = {};
        		var url = "mobileApps/updateTruckIsActiveFlag";

        		var truckDetails=localStorageService.get('truckDetails');

        		reqData={
        			license:truckDetails.license,
        			isActiveFlag:"No"
        		}

        		adapter.getServiceData(reqData, url).then(success, error);


        		function success(result) {
        			//alert(JSON.stringify(result));
        			localStorageService.set('truckDetails',null);
        			scheduleFactory.setTruckDetails(null);

        				$state.go('ConnectTruck');
        		}

        		function error(message) {
        			console.log(message);

        		}
    }

}]);