app.controller("loginController", ['$scope', '$http', '$state', '$cookies', '$cookieStore', 'localStorageService', 'adapter', 'scheduleFactory', function($scope, $http, $state, $cookies, $cookieStore, localStorageService, adapter, scheduleFactory) {


    var inAppBrowserRef;
    if ($cookieStore != null) {
        $scope.inputUsername = $cookieStore.get('loggedUser');
        $scope.inputPassword = $cookieStore.get('password');
        $scope.rememberMe = true;
    }

    $scope.loginErrorMessage="";
    $scope.loginError = false;
    $scope.showText = true;

    $scope.loginSubmit = function(status) {
        if (!status) {
            return false;
        }

        $scope.loginError = false;
        $scope.showText = false;



        var reqData = {
            inputUsername: $scope.inputUsername,
            inputPassword: $scope.inputPassword
        }



        var isOffline = 'onLine' in navigator && !navigator.onLine;

        localStorageService.set('isOffline', isOffline);

        if (!isOffline) {
            //reqData=$.param(reqData);

            var url = "mobileUser/login";

            adapter.getServiceData(reqData, url).then(success, error);

            function success(result) {

            console.log(result.errorMessage);

                if (result.user != null) {
                    if ($scope.rememberMe) {
                        $cookies.loggedUser = $scope.inputUsername;
                        $cookies.password = $scope.inputPassword;
                        $cookieStore.put('loggedUser', $cookies.loggedUser);
                        $cookieStore.put('password', $cookies.password);
                    }
                    $scope.loginError = false;
                    $scope.showText = true;

                    //console.log(JSON.stringify(result));
                    app.saveLoginDetails($scope.inputUsername, $scope.inputPassword, result.user,successCallback);
                    localStorageService.set("userId", result.user.user_ID);
                    localStorageService.set("userName", result.user.name);
                    //$state.go('MobileDashboard');


                    //var target = "_blank";

                    //var options = "location=yes";
                    //inAppBrowserRef = cordova.InAppBrowser.open('https://dev-693134.oktapreview.com/', target, options);
                    //inAppBrowserRef.addEventListener('loadstart',loadingUrl);
                    //inAppBrowserRef.addEventListener('exit', exitCallback);

                    //var ref = cordova.InAppBrowser.open('https://dev-693134.oktapreview.com/', '_blank', 'location=yes');
                } else {
                    $scope.loginError = true;
                    $scope.loginErrorMessage=result.errorMessage;
                    //$scope.loginError=false;
                    $scope.showText = true;
                }


            }

            function error(error) {
                app.loginOffline($scope.inputUsername, $scope.inputPassword, suucessLogin, errorLogin);

                            function suucessLogin(data) {
                                $scope.loginError = false;
                                $scope.showText = true;
                                /*localStorageService.set('loggedOut',"loggedIn");*/

                                scheduleFactory.setTruckDetails(localStorageService.get('truckDetails'));

                                $scope.truckDetails = scheduleFactory.getTruckDetails();

                                $scope.$apply();
                                localStorageService.set('whichSchedule', 'current');

                                if ($scope.truckDetails !== null && $scope.truckDetails !== undefined) {
                                    $state.go('Schedule');
                                } else {
                                    $state.go('ConnectTruck');
                                }

                            }

                            function errorLogin(message) {
                                console.log(message);
                                $scope.loginError = true;
                                //$scope.loginError=false;
                                $scope.loginErrorMessage="Please Check Credentials and try again".
                                $scope.showText = true;
                                $scope.$apply();

                            }
            }
        } else {

            app.loginOffline($scope.inputUsername, $scope.inputPassword, suucessLogin, errorLogin);

            function suucessLogin(data) {
                $scope.loginError = false;
                $scope.showText = true;
				/*localStorageService.set('loggedOut',"loggedIn");*/
                scheduleFactory.setTruckDetails(localStorageService.get('truckDetails'));

                $scope.truckDetails = scheduleFactory.getTruckDetails();

                $scope.$apply();
                localStorageService.set('whichSchedule', 'current');

                if ($scope.truckDetails !== null && $scope.truckDetails !== undefined) {
                    $state.go('Schedule');
                } else {
                    $state.go('ConnectTruck');
                }

            }

            function errorLogin(message) {
                console.log(message);
                $scope.loginError = true;
                //$scope.loginError=false;
                $scope.showText = true;
                $scope.$apply();

            }

        }

    }


    function exitCallback() {

        var status = localStorageService.get("status");

        if (status === "success") {
            $state.go('MobileDashboard');
            localStorageService.set("status", null);
        }


    }

    function successCallback(data){

    var truckDetails=localStorageService.get('truckDetails');

    var loggedOut = localStorageService.get('loggedOut');

    if(truckDetails!==null && truckDetails!==undefined && loggedOut!=="loggedOut" ){
    	scheduleFactory.setTruckDetails(localStorageService.get('truckDetails'));
    	localStorageService.set('loggedOut',"loggedIn");

    	syncMobileData();


    }else{
    localStorageService.set('loggedOut',"loggedIn");
    	$state.go('ConnectTruck');
    }

    }

    function syncMobileData(){
    app.fetchAllData(successCall, errorCall);
    }

    function successCall(data) {

            if (data !== null && data !== undefined && data.length > 0) {
                var url = "mobileApps/syncDataFromMob";

                var options = {
                    dimBackground: true
                };

                SpinnerPlugin.activityStart("Syncing data", options);


                adapter.getServiceData(data, url).then(success, error);

                function success(result) {
                    SpinnerPlugin.activityStop();
                    //console.log(result);
                    if (result !== null && result !== undefined && result.length > 0) {
                        app.deleteAllData(result);
                        $state.go('MobileDashboard');
                    }

                }

                function error(error) {
                    SpinnerPlugin.activityStop();
                    $state.go('MobileDashboard');
                    console.log(error);
                }
            }

        }

        function errorCall(error) {
    		$state.go('MobileDashboard');
        }

    function loadingUrl(event) {
        if (event.url !== "https://dev-693134.oktapreview.com/") {
            alert(event.url);
            alert(event.url.substring(getPosition(event.url, "=", 2) + 1, getPosition(event.url, "&", 1)));

            localStorageService.set("status", "success");
            inAppBrowserRef.close();
        }
    }

    function getPosition(str, m, i) {
        return str.split(m, i).join(m).length;
    }


    document.addEventListener('backbutton', function(event){
    if($state.current.name==='Login'){
    	event.preventDefault(); // EDIT
	    navigator.app.exitApp(); // exit the app
    }

    });

}]);