app.controller("dashboardController", ['$scope', '$rootScope', '$http', '$q', '$state', 'localStorageService', 'scheduleFactory', 'adapter', function($scope, $rootScope, $http, $q, $state, localStorageService, scheduleFactory, adapter) {


    var serviceReqData = {};

    $scope.loader = false;

    $scope.goToSchedule = function(schedule) {
    if($scope.dashboardData.nextTripCount!==null && $scope.dashboardData.nextTripCount!==undefined && $scope.dashboardData.nextTripCount>0){
    	localStorageService.set('whichSchedule', schedule);
        $state.go('Schedule');
    }


    }

    $scope.truckDetails = scheduleFactory.getTruckDetails();

    $scope.goToMySchedule = function(tabName) {
        localStorageService.set('scheduleTab', tabName);
        $state.go('MySchedule');
    }

    $scope.goToPendingRequest = function() {
        $state.go('PendingRequests')
    }

    var reqData = {};
    var url = "mobileApps/getDashboardInMobApps/" + $scope.truckDetails.truckId;

    $scope.loader = true;

    adapter.getServiceDataGet(reqData, url).then(success, error);

    function success(data) {
        $scope.dashboardData = data;

        $rootScope.pendingCount = data.nextTripCount;

        localStorageService.set('upcomingSchedules', data.upcomingConfScheduleList);
        localStorageService.set('pastSchedules', data.pastMonthScheduleList);

        $scope.loader = false;
    }

    function error(message) {
        $scope.loader = false;
    }

}]);