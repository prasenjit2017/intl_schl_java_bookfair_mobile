app.controller("headerController", ['$scope', '$http', '$state', '$rootScope', '$cordovaNetwork', 'localStorageService', '$timeout', 'adapter', function($scope, $http, $state, $rootScope, $cordovaNetwork, localStorageService, $timeout, adapter) {

    var menuclicked = false;
    var onlineNetwork=true;

    var isOffline = 'onLine' in navigator && !navigator.onLine;

    if(isOffline){
    	onlineNetwork=false;
    }
    $scope.offline = isOffline;

    $scope.userName = localStorageService.get("userName");

    $scope.linkClicked = function(state) {

        if (state === 'dashboard') {
            if ($state.current.name !== 'MobileDashboard') {
                $state.go('MobileDashboard');
            }
        } else if (state === 'support') {
            if ($state.current.name !== 'Support') {
                $state.go('Support');
            }
        } else if (state === 'profile') {
            if ($state.current.name !== 'MyProfile') {
                $state.go('MyProfile');
            }
        } else if (state === 'requests') {
            if ($state.current.name !== 'PendingRequests') {
                $state.go('PendingRequests');
            }
        } else if (state === 'mySchedule') {
            if ($state.current.name !== 'MySchedule') {
                $state.go('MySchedule');
            }
        } else if (state === 'logout') {
        	logOut();
            //$state.go('Login');
        } else if (state === 'settings') {
            $state.go('Settings');
        }

    }

    function logOut(){
    	var reqData = {};
		var url = "mobileApps/updateTruckIsActiveFlag";

		var truckDetails=localStorageService.get('truckDetails');

		reqData={
			license:truckDetails.license,
			isActiveFlag:"No"
		}

		adapter.getServiceData(reqData, url).then(success, error);


		function success(result) {
			//alert(JSON.stringify(result));
			localStorageService.set('loggedOut',"loggedOut");
			//localStorageService.set('truckDetails',null);

				$state.go('Login');
		}

		function error(message) {

			console.log(message);
			if(isOffline){
				/*localStorageService.set('loggedOut',"loggedOut");*/
				$state.go('Login');
			}

		}
    }

    $scope.showNotification = function() {
        if ($state.current.name !== 'Notification') {
            $state.go('Notification');
        }

    }

    $('.navbar-collapse').on('shown.bs.collapse', function() {
        // do something
        $('#navbar-header').addClass('menu-open-collapse');
    });

    $('.navbar-collapse').on('hidden.bs.collapse', function() {
        // do something
        $('#navbar-header').removeClass('menu-open-collapse');
    });


    $scope.$on('$cordovaNetwork:online', function(event, networkState) {
        $scope.offline = false;
		if(!onlineNetwork){
			app.fetchAllData(successCall, errorCall);
			onlineNetwork=true;
		}


    });

    function successCall(data) {
        if (data !== null && data !== undefined && data.length > 0) {
            var url = "mobileApps/syncDataFromMob";

            var options = {
                dimBackground: true
            };

            SpinnerPlugin.activityStart("Syncing data", options);


            adapter.getServiceData(data, url).then(success, error);

            function success(result) {
                SpinnerPlugin.activityStop();
                //console.log(result);
                if (result !== null && result !== undefined && result.length > 0) {
                    app.deleteAllData(result);
                }

            }

            function error(error) {
                SpinnerPlugin.activityStop();
                console.log(error);
            }
        }
    }

    function errorCall(error) {

    }

    $scope.$on('$cordovaNetwork:offline', function(event, networkState) {
        $scope.offline = true;
        onlineNetwork=false;

    });

    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        $timeout(function() {

            var userId = localStorageService.get("userId");
            switch ($state.current.name) {
                case "MobileDashboard":
                    $('#link-dashboard').addClass('link-hover');
                    $('#link-myschedule').removeClass('link-hover');
                    $('#link-support').removeClass('link-hover');
                    $('#link-profile').removeClass('link-hover');
                    $('#link-requests').removeClass('link-hover');
                    break;

                case "MySchedule":
                    $('#link-myschedule').addClass('link-hover');
                    $('#link-dashboard').removeClass('link-hover');
                    $('#link-support').removeClass('link-hover');
                    $('#link-profile').removeClass('link-hover');
                    $('#link-requests').removeClass('link-hover');
                    break;
                case "Support":
                    $('#link-myschedule').removeClass('link-hover');
                    $('#link-dashboard').removeClass('link-hover');
                    $('#link-support').addClass('link-hover');
                    $('#link-profile').removeClass('link-hover');
                    $('#link-requests').removeClass('link-hover');
                    break;
                case "MyProfile":
                    $('#link-myschedule').removeClass('link-hover');
                    $('#link-dashboard').removeClass('link-hover');
                    $('#link-support').removeClass('link-hover');
                    $('#link-profile').addClass('link-hover');
                    $('#link-requests').removeClass('link-hover');
                    break;

                case "PendingRequests":
                    $('#link-myschedule').removeClass('link-hover');
                    $('#link-dashboard').removeClass('link-hover');
                    $('#link-support').removeClass('link-hover');
                    $('#link-profile').removeClass('link-hover');
                    $('#link-requests').addClass('link-hover');
                    break;



            }
        });
    });

    $(function(){
    	$(document).on('click',".humberger-button",function(){
    		setTimeout(function(){
    			var calculatedTop = $(".navbar-header").outerHeight() +  $(".navbar-nav").height()
    			$(".main_nav .overlay").css('top',calculatedTop + 'px');
    		},500)
    	});
    });

}]);