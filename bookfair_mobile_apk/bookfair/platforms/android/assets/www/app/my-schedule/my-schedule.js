app.controller("myScheduleController", ['$scope', '$http', '$q', '$state', 'localStorageService', 'scheduleFactory', function($scope, $http, $q, $state, localStorageService, scheduleFactory) {

    if (localStorageService.get('scheduleTab') !== null) {
        if (localStorageService.get('scheduleTab') === 'past') {
            $scope.showPast = true;
            $scope.showUpcoming = false;
            getPastScheduleDetails();
        } else if (localStorageService.get('scheduleTab') === 'upcoming') {
            $scope.showPast = false;
            $scope.showUpcoming = true;
            getUpcomingScheduleDetails();
        }
        localStorageService.set('scheduleTab', null);
    } else {
        $scope.showPast = false;
        $scope.showUpcoming = true;
        getUpcomingScheduleDetails();

    }


    $scope.truckDetails = scheduleFactory.getTruckDetails();

    $scope.quantity=4;




    function getPastScheduleDetails() {

        $scope.requestDetails = localStorageService.get('pastSchedules');
        if ($scope.requestDetails !== null) {
        	$scope.totalRequest=$scope.requestDetails.length;
            for (var i = 0; i < $scope.requestDetails.length; i++) {
                var deliveryAreaList = [];
                var noOfPickUp = 0;
                var noOfDelivery = 0;
                for (var j = 0; j < $scope.requestDetails[i].sequenceList.length; j++) {
                    deliveryAreaList.push($scope.requestDetails[i].sequenceList[j].deliveryAreaName);
                    if ($scope.requestDetails[i].sequenceList[j].delivery === "Delivery") {
                        noOfDelivery++;
                    } else {
                        noOfPickUp++;
                    }
                }
                $scope.requestDetails[i].deliveryAreaList = deliveryAreaList;
                $scope.requestDetails[i].noOfDelivery = noOfDelivery;
                $scope.requestDetails[i].noOfPickUp = noOfPickUp;
            }
        } else {
            $scope.requestDetails = [];
        }

    }

    function getUpcomingScheduleDetails() {

        $scope.requestDetails = localStorageService.get('upcomingSchedules');

        if ($scope.requestDetails !== null) {
        $scope.totalRequest=$scope.requestDetails.length;
            for (var i = 0; i < $scope.requestDetails.length; i++) {
                var deliveryAreaList = [];
                var noOfPickUp = 0;
                var noOfDelivery = 0;
                for (var j = 0; j < $scope.requestDetails[i].sequenceList.length; j++) {
                    deliveryAreaList.push($scope.requestDetails[i].sequenceList[j].deliveryAreaName);
                    if ($scope.requestDetails[i].sequenceList[j].delivery === "Delivery") {
                        noOfDelivery++;
                    } else {
                        noOfPickUp++;
                    }
                }
                $scope.requestDetails[i].deliveryAreaList = deliveryAreaList;
                $scope.requestDetails[i].noOfDelivery = noOfDelivery;
                $scope.requestDetails[i].noOfPickUp = noOfPickUp;
            }
        } else {
            $scope.requestDetails = [];
        }

    }


    $scope.tabClick = function(tabName) {
        if (tabName === 'past') {
            $scope.showPast = true;
            $scope.showUpcoming = false;
            getPastScheduleDetails();
        } else if (tabName === 'upcoming') {
            $scope.showPast = false;
            $scope.showUpcoming = true;
            getUpcomingScheduleDetails();
        }
    }




    $scope.goToSchedule = function(index) {
        if ($scope.showPast) {
            localStorageService.set('whichSchedule', 'past');
        } else {
            localStorageService.set('whichSchedule', 'upcoming');
        }

        scheduleFactory.setCompleteData($scope.requestDetails[index]);
        $state.go('Schedule');
    }

    $scope.loadMore=function(){
		$scope.quantity=quantity+4;
    }

}]);