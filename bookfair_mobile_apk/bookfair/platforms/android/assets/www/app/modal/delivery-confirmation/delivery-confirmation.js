app.controller("deliveryConfirmationCtrl", ['$scope', '$uibModalInstance', '$state', 'localStorageService', function($scope, $uibModalInstance, $state, localStorageService) {
    $scope.hidegreenTick = true;
    $scope.hideRedCross = false;
    modalObj = localStorageService.get('modalObj');
    if (modalObj !== null) {
        $scope.modalEntity = modalObj.modalEntity;
        $scope.modalBtn = modalObj.modalBtnText;
        $scope.modalBody = modalObj.body;
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss();
    };

    $scope.ok = function(str) {
        localStorageService.set('modalObj', null);
        localStorageService.set('deliveryFailureReason', $scope.reason);
        $uibModalInstance.close(str);
    };

    $scope.closeModal = function() {
        $uibModalInstance.dismiss();
    }

}]);