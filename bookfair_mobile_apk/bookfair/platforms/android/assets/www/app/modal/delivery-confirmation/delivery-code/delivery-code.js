app.controller("deliveryCodeCtrl", ['$scope', '$uibModalInstance', '$state', 'localStorageService', 'scheduleFactory', function($scope, $uibModalInstance, $state, localStorageService, scheduleFactory) {
    $scope.firstDiv = true;
    $scope.helpDiv = false;
    $scope.codeEntry = false;
    $scope.singleConfirmation = false;
    $scope.reasonEntry = false;
    $scope.errorBarcodeReason = false;
    $scope.errorBarcodeCount = false;

    var prodDeliveryDetails = [];
    var eftposBarcodeList = [];
    $scope.codeArray = [];
    $scope.currentIndex = localStorageService.get('currentIndex');

    var scheduleData = scheduleFactory.getCompleteData();
    $scope.currentIndex = localStorageService.get('currentIndex');
    $scope.recordToSave = scheduleData.sequenceList[$scope.currentIndex];
    $scope.productDetails = $scope.recordToSave.itemDetailsList;
    $scope.prodIndex = localStorageService.get('productRowIndex');

    localStorageService.set('eftposBarcodeFailureCount', 0);
    localStorageService.set('eftposBarcodeFailureReason', "");
    scheduleFactory.setEftposTruckList(null);




    $scope.product = $scope.productDetails[$scope.prodIndex];
    $scope.remain = $scope.product.reservedQuantity;

    $scope.itemIndex = 0;

    $scope.cancel = function() {
        $uibModalInstance.dismiss();
    };


    $scope.closeModal = function() {
        if ($scope.firstDiv || $scope.codeEntry || $scope.singleConfirmation || $scope.reasonEntry) {
            localStorageService.set('eftposBarcodeFailureCount', 0);
            localStorageService.set('eftposBarcodeFailureReason', "");
            $uibModalInstance.close("closed");
        } else if ($scope.helpDiv) {
            $scope.helpDiv = false;
            if ($scope.lastDiv === "firstDiv") {
                $scope.firstDiv = true;
            } else if ($scope.lastDiv === "singleConfirmation") {
                $scope.singleConfirmation = true;
            } else if ($scope.lastDiv === "codeEntry") {
                $scope.codeEntry = true;
            } else if($scope.lastDiv ==="issueResolved"){
            	$scope.issueResolved =true;
            }
             else {
                $scope.reasonEntry = true;
            }
        } else if ($scope.issueResolved) {
            $uibModalInstance.close("success");
        }

    }

    $scope.showHelp = function() {

        if ($scope.firstDiv) {
            $scope.lastDiv = "firstDiv";
        } else if ($scope.singleConfirmation) {
            $scope.lastDiv = "singleConfirmation";
        } else if ($scope.codeEntry) {
            $scope.lastDiv = "codeEntry";
        } else if ($scope.reasonEntry) {
            $scope.lastDiv = "reasonEntry";
        }else if ($scope.issueResolved){
        	$scope.lastDiv= "issueResolved";
        }

        $scope.helpDiv = true;
        $scope.firstDiv = false;
        $scope.singleConfirmation = false;
        $scope.codeEntry = false;
        $scope.reasonEntry = false;
        $scope.issueResolved=false;
    }

    $scope.manuallyEnterCode = function() {
        $scope.codeEntry = true;
        $scope.firstDiv = false;
        $scope.singleConfirmation = false;
        $scope.reasonEntry = false;
    }

    // var codeCount=0;

    $scope.codeValidate = function(code) {

        if (code !== "" && code !== null && code !== undefined) {
            $scope.codeEntry = false;
            $scope.firstDiv = false;

            $scope.codeArray.push(code);

            // codeCount++;

            $scope.productDetails[$scope.prodIndex].codeArray = $scope.codeArray;

            var singleObj = {
                itemID: $scope.productDetails[$scope.prodIndex].itemId,
                barcode: code
            };

            eftposBarcodeList.push(singleObj);
            $scope.remain = $scope.remain - 1;
			$scope.itemIndex++;


            if ($scope.product.reservedQuantity === $scope.itemIndex) {

                scheduleFactory.setEftposTruckList(eftposBarcodeList);

                localStorageService.set('eftposBarcodeFailureCount', $scope.eftposBarcodeFailureCount);
                localStorageService.set('eftposBarcodeFailureReason', $scope.eftposBarcodeFailureReason);

                $scope.issueResolved = true;
                prodDeliveryDetails = [];
                $scope.codeArray = [];
                eftposBarcodeList = [];
                $uibModalInstance.close("success");

            } else {
                $scope.singleConfirmation = true;
            }
        }

    }

    $scope.enterReason = function() {
        $scope.reasonEntry = true;

        $scope.getNumber($scope.remain);

        $scope.firstDiv = false;
        $scope.helpDiv = false;
        $scope.codeEntry = false;
        $scope.singleConfirmation = false;
    }

    $scope.nextMachine = function() {
        if ($scope.product.reservedQuantity === $scope.itemIndex) {

            //$uibModalInstance.closeModal();
            $scope.codeEntry = false;
            $uibModalInstance.close("success");

        } else {
            $scope.codeEntry = true;
            $scope.manualCode = "";
            $scope.singleConfirmation = false;
            $scope.reasonEntry = false;

        }


    }

    $scope.nextProdMachine = function() {
        if ($scope.product.reservedQuantity === $scope.itemIndex) {
            $uibModalInstance.close("success");
        } else {
            $scope.codeEntry = true;
            $scope.manualCode = "";
            $scope.singleConfirmation = false;
            $scope.issueResolved = false;
            $scope.reasonEntry = false;
        }
    }

    $scope.getNumber = function(num) {
        $scope.numArray = [];
        for (var i = 1; i <= $scope.remain; i++) {
            $scope.numArray.push(i);
        }
    }

    $scope.failureResolved = function() {

        if ($scope.eftposBarcodeFailureReason === "" || $scope.eftposBarcodeFailureReason === null || $scope.eftposBarcodeFailureReason === undefined) {
            $scope.errorBarcodeReason = true;
        } else {
            $scope.errorBarcodeReason = false;
        }

        if ($scope.eftposBarcodeFailureCount === "" || $scope.eftposBarcodeFailureCount === null || $scope.eftposBarcodeFailureCount === undefined) {
            $scope.errorBarcodeCount = true;
        } else {
            $scope.errorBarcodeCount = false;
        }

        if (!$scope.errorBarcodeReason && !$scope.errorBarcodeCount) {
            $scope.itemIndex = $scope.itemIndex + parseInt($scope.eftposBarcodeFailureCount);
            $scope.remain = $scope.remain - parseInt($scope.eftposBarcodeFailureCount);

            if (localStorageService.get('eftposBarcodeFailureCount') !== null || localStorageService.get('eftposBarcodeFailureCount') !== undefined) {
                $scope.eftposBarcodeFailureCount = parseInt(localStorageService.get('eftposBarcodeFailureCount')) + parseInt($scope.eftposBarcodeFailureCount);
                localStorageService.set('eftposBarcodeFailureCount', parseInt($scope.eftposBarcodeFailureCount));
            } else {
                localStorageService.set('eftposBarcodeFailureCount', parseInt($scope.eftposBarcodeFailureCount));
            }
            localStorageService.set('eftposBarcodeFailureCount', parseInt($scope.eftposBarcodeFailureCount));
            localStorageService.set('eftposBarcodeFailureReason', $scope.eftposBarcodeFailureReason);

            if ($scope.product.reservedQuantity === $scope.itemIndex || $scope.product.reservedQuantity < $scope.itemIndex) {
                scheduleFactory.setEftposTruckList(eftposBarcodeList);
                $uibModalInstance.close("success");
            } else {
                $scope.reasonEntry = false;
                $scope.issueResolved = true;
            }

        }



    }

}]);