app.controller("deliveryController", ['$scope', '$http', '$state', '$uibModal', 'localStorageService', 'scheduleFactory', '$cordovaNetwork', 'adapter', function($scope, $http, $state, $uibModal, localStorageService, scheduleFactory, $cordovaNetwork, adapter) {

    var errorName = 'DeliveryError';
    $scope.showCode = false;

    $scope.recordToSave = localStorageService.get('recordToSave');

    $scope.completeData = scheduleFactory.getCompleteData();
    var isOffline = 'onLine' in navigator && !navigator.onLine;

    $scope.currentIndex = localStorageService.get('currentIndex');
    $scope.totalDeliveryLength = localStorageService.get('totalDeliveryLength');

    $scope.destinationData = $scope.completeData.sequenceList[$scope.currentIndex];

    for(var i=0;i<$scope.destinationData.itemDetailsList.length;i++){
    	$scope.destinationData.itemDetailsList[i].deliveryCheck=false;

    }

    $scope.imageList = [];
    localStorageService.set('eftposBarcodeFailureCount', 0);
    localStorageService.set('eftposBarcodeFailureReason', null);
    localStorageService.set('deliveryFailureReason', null);
    scheduleFactory.setEftposTruckList(null);

    var itemCheckList = [];


    var todayDate = new Date();


                var month = todayDate.getMonth() + 1;
                if (month < 10) {
                    month = "0" + month;
                }

                var tarikh=todayDate.getDate();
                if(tarikh<10){
                	tarikh="0" + tarikh;
                }

                 todayDate=todayDate.getFullYear()+"-"+month+"-"+tarikh;
               // var todayDate = $scope.todayDate.getDate() + "-" + month + "-" + $scope.todayDate.getFullYear();

    app.fetchScheduleData(successCallback, errorCallback);



    $scope.confirmDestination = function() {

        var count = 0;
        getItems();

        for (var i = 0; i < $scope.destinationData.itemDetailsList.length; i++) {
            if ($scope.destinationData.itemDetailsList[i].deliveryCheck) {
                count++;
            } else {
                $scope.saveDraft();
                break;
            }
        }
        if (count === $scope.destinationData.itemDetailsList.length) {
            /*alert("All item checked");*/
            var singleRow = {
                scheduleID: $scope.completeData.scheduleID,
                bookfairID: $scope.destinationData.bookfairId,
                sequenceID: $scope.destinationData.sequenceId,
                completedDate:todayDate,
                itemCheckList: itemCheckList,
                eftposBarcodeList: scheduleFactory.getEftposTruckList(),
                eftposBarcodeFailureCount: localStorageService.get('eftposBarcodeFailureCount'),
                eftposBarcodeFailureReason: localStorageService.get('eftposBarcodeFailureReason'),
                deliveryInstruction: $scope.deliveryInstruction,
                deliveryFailureReason: localStorageService.get('deliveryFailureReason'),
                imageList: $scope.imageList

            }

            if (isOffline) {
                scheduleFactory.addRecord(singleRow);
                $scope.destinationData.status = "Completed";
                $scope.completeData.sequenceList[$scope.currentIndex] = $scope.destinationData;
                scheduleFactory.setCompleteData($scope.completeData);
                $scope.userData = $scope.completeData;

                app.updateScheduleData1($scope.userData, successCallbackUpdate, errorCallbackUpdate);
            } else {

                var offlineSyncData = [];
                offlineSyncData.push(singleRow);
                var url = "mobileApps/syncDataFromMob";

                var reqData = {
                    offlineSyncData: offlineSyncData
                };

			 var options = {
							dimBackground: true
						};

						SpinnerPlugin.activityStart("Syncing data", options);

                adapter.getServiceData(offlineSyncData, url).then(success, error);

                function success(result) {
                    //console.log(result);
                    SpinnerPlugin.activityStop();
                    $scope.destinationData.status = "Completed";
                    $scope.completeData.sequenceList[$scope.currentIndex] = $scope.destinationData;
                    scheduleFactory.setCompleteData($scope.completeData);
                    $scope.userData = $scope.completeData;
                    SpinnerPlugin.activityStop();

                    app.updateScheduleData1($scope.userData, successCallbackUpdate, errorCallbackUpdate);
                }

                function error(error) {
                    console.log(error);
                    SpinnerPlugin.activityStop();
                    scheduleFactory.addRecord(singleRow);
                    $scope.destinationData.status = "Completed";
                    $scope.completeData.sequenceList[$scope.currentIndex] = $scope.destinationData;
                    scheduleFactory.setCompleteData($scope.completeData);
                    $scope.userData = $scope.completeData;

                    app.updateScheduleData1($scope.userData, successCallbackUpdate, errorCallbackUpdate);
                }

            }




        }
    }

    function successCallback(data) {
        $scope.userData = data;
        $scope.$apply();
    }

    function errorCallback(message) {

    }


    function successCallbackUpdate(message) {
        //alert(message);



        if ($scope.currentIndex < $scope.totalDeliveryLength - 1) {
            var doNotShow = localStorageService.get('doNotShowAgain');



            if (doNotShow) {
                $scope.currentIndex = $scope.currentIndex + 1;

                for (var i = $scope.currentIndex; i < $scope.totalDeliveryLength; i++) {
                    if ($scope.completeData.sequenceList[i].status === 'Driver Accepted' || $scope.completeData.sequenceList[i].status === undefined || $scope.completeData.sequenceList[i].status === 'Ongoing') {
                        localStorageService.set('currentIndex', i);
                        $state.go('Destination');
                        break;
                    } else {
                        if (i == $scope.totalDeliveryLength - 1) {

                        	/*$scope.completeData.status = "Completed";
                        	$scope.$apply();
                        	scheduleFactory.setCompleteData($scope.completeData);*/

                        	//app.updateScheduleData1($scope.completeData, successCallbackUpdate1, errorCallbackUpdate);
							$state.go('Schedule');
                            break;
                        }
                    }

                }


            } else {
                $state.go('DestinationComplete');
            }
        } else {
			/*$scope.completeData.status = "Completed";
			$scope.$apply();
			scheduleFactory.setCompleteData($scope.completeData);

			localStorageService.set("whichSchedule","");
			app.updateScheduleData1($scope.completeData, successCallbackUpdate1, errorCallbackUpdate);*/
			$state.go('Schedule');
            //$state.go('Schedule');
        }
    }

    function successCallbackUpdate1(message){
    	$state.go('Schedule');
    }

    function errorCallbackUpdate(message) {
        alert(message);
    }


    $scope.saveDraft = function() {
        modalEntity = "All items in the trip must be delivered/picked-up.Please go back and check the items before ending the trip. ";
        modalBody = "If you are unable to deliver/pick up the unchecked items, please tell us why";
        modalOpenDraft("success");


    }
    /* MODAL IMPLEMENTATION */
    $scope.animationsEnabled = true;

    $scope.openDraft = function() {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/modal/delivery-confirmation/delivery-confirmation.html',
            controller: 'deliveryConfirmationCtrl',
            windowClass: 'center-modal',
            backdrop: 'static',
            keyboard: false,
            size: 'lg'
        }).result.then(function(data) {
            if (data === "confirm") {
                var reason = localStorageService.get('deliveryFailureReason');
                var singleRow = {
                    scheduleID: $scope.completeData.scheduleID,
                    bookfairID: $scope.destinationData.bookfairId,
                    sequenceID: $scope.destinationData.sequenceId,
                    completedDate:todayDate,
                    itemCheckList: itemCheckList,
                    eftposBarcodeList: scheduleFactory.getEftposTruckList(),
                    eftposBarcodeFailureCount: localStorageService.get('eftposBarcodeFailureCount'),
                    eftposBarcodeFailureReason: localStorageService.get('eftposBarcodeFailureReason'),
                    deliveryInstruction: $scope.deliveryInstruction,
                    deliveryFailureReason: localStorageService.get('deliveryFailureReason'),
                    imageList: $scope.imageList

                }


                if (isOffline) {
                    scheduleFactory.addRecord(singleRow);
                    $scope.destinationData.status = "Completed";
                    $scope.completeData.sequenceList[$scope.currentIndex] = $scope.destinationData;
                    scheduleFactory.setCompleteData($scope.completeData);
                    $scope.userData = $scope.completeData;

                    app.updateScheduleData1($scope.userData, successCallbackUpdate, errorCallbackUpdate);
                } else {

                    var offlineSyncData = [];
                    offlineSyncData.push(singleRow);
                    var url = "mobileApps/syncDataFromMob";

                    var reqData = {
                        offlineSyncData: offlineSyncData
                    };

                    adapter.getServiceData(offlineSyncData, url).then(success, error);

                    function success(result) {
                        //console.log(result);
                        SpinnerPlugin.activityStop();
                        $scope.destinationData.status = "Completed";
                        $scope.completeData.sequenceList[$scope.currentIndex] = $scope.destinationData;
                        scheduleFactory.setCompleteData($scope.completeData);
                        $scope.userData = $scope.completeData;

                        app.updateScheduleData1($scope.userData, successCallbackUpdate, errorCallbackUpdate);
                    }

                    function error(error) {
                        console.log(error);
                        SpinnerPlugin.activityStop();
                        scheduleFactory.addRecord(singleRow);
                        $scope.destinationData.status = "Completed";
                        $scope.completeData.sequenceList[$scope.currentIndex] = $scope.destinationData;
                        scheduleFactory.setCompleteData($scope.completeData);
                        $scope.userData = $scope.completeData;

                        app.updateScheduleData1($scope.userData, successCallbackUpdate, errorCallbackUpdate);
                    }

                }


            }

        });
    };

    function modalOpenDraft(modalType) {
        if (errorName !== null && errorName !== "") {
            if (modalType === "success") {
                var modalObj = {
                    "modalEntity": modalEntity,
                    "body": modalBody,
                    "modalBtnText": "Back to Manage Contract",
                    "backLink": "OK",
                    "singleBtn": true
                };
            } else {
                var modalObj = {
                    "modalEntity": "Failed!! ",
                    "body": errorMessage,
                    "modalBtnText": "",
                    "backLink": "OK",
                    "singleBtn": true
                };
            }

            localStorageService.set("modalObj",
                modalObj);
            $scope.openDraft();
        }
    }


    $scope.checkItem = function(item, index) {
        item.deliveryCheck = true;

        if (item.itemName.toLowerCase() === 'eftpos' && item.reservedQuantity>0) {
            localStorageService.set("productRowIndex", index);
            $scope.codeEntry();
        }

    }

    $scope.codeEntry = function() {

        modalOpenDelivery();

    }

    function modalOpenDelivery() {

        if (errorName !== null && errorName !== "") {
            $scope.openCodeModal();
        }

    }

    $scope.openCodeModal = function() {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/modal/delivery-confirmation/delivery-code/delivery-code.html',
            controller: 'deliveryCodeCtrl',
            windowClass: 'center-modal',
            backdrop: 'static',
            keyboard: false,
            size: 'lg'
        }).result.then(function(data) {
            if (data === 'success') {
                //$scope.recordToSave=localStorageService.get('recordToSave');

            } else if (data === 'closed') {
                for (var i = 0; i < $scope.destinationData.itemDetailsList.length; i++) {
                    if ($scope.destinationData.itemDetailsList[i].itemName.toLowerCase() === "eftpos") {
                        $scope.destinationData.itemDetailsList[i].deliveryCheck = false;
                        $scope.destinationData.itemDetailsList[i].codeArray = [];
                        $scope.$apply();
                        break;
                    }
                }
            }
        });


    }

    $scope.takePhoto = function() {

        if ($scope.imageList.length < 6) {
            navigator.camera.getPicture(onSuccess, onFail, {
                quality: 50,
                destinationType: Camera.DestinationType.DATA_URL
            });

            function onSuccess(imageData) {


                var image = document.getElementById('image-area');

                //var byteImageUrl=base64ToArrayBuffer(imageData);

                var x = document.createElement("IMG");
                x.setAttribute("src", "data:image/jpeg;base64," + imageData);
                x.setAttribute("width", "100");
                x.setAttribute("height", "100");
                x.setAttribute("alt", "img");
                x.style.padding = "3px 0px 0px 3px";
                //image.src = "data:image/jpeg;base64," + imageData;

                document.getElementById('image-area').appendChild(x);

                $scope.imageList.push(imageData);
            }

            function onFail(message) {
                //alert('Failed because: ' + message);
            }
        }

    }

    function base64ToArrayBuffer(base64) {
        var binary_string =  window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array( len );
        for (var i = 0; i < len; i++)        {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }

    $scope.getPicture = function() {

        if ($scope.imageList.length < 6) {
            navigator.camera.getPicture(onSuccess, onFail, {
                quality: 50,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY
            });

            function onSuccess(imageURL) {
                var image = document.getElementById('image-area');
                //image.src = "data:image/jpeg;base64," +imageURL;
                var x = document.createElement("IMG");
                x.setAttribute("src", "data:image/jpeg;base64," + imageURL);
                x.setAttribute("width", "100");
                x.setAttribute("height", "100");
                x.setAttribute("alt", "img");
                x.style.padding = "3px 0px 0px 3px";
                //image.src = "data:image/jpeg;base64," + imageData;

                $scope.imageList.push(imageURL);

                document.getElementById('image-area').appendChild(x);
            }

            function onFail(message) {
                // alert('Failed because: ' + message);
            }
        }

    }

    function getItems() {




        for (var i = 0; i < $scope.destinationData.itemDetailsList.length; i++) {

            var singleItem = {
                itemId: $scope.destinationData.itemDetailsList[i].itemId,
                itemName: $scope.destinationData.itemDetailsList[i].itemName,
                quantity: $scope.destinationData.itemDetailsList[i].reservedQuantity,
                flag: ($scope.destinationData.itemDetailsList[i].itemName.toLowerCase() === "eftpos") ? false : $scope.destinationData.itemDetailsList[i].deliveryCheck
            }

            itemCheckList.push(singleItem);

        }
    }

}]);