app.controller("pendingRequestsController", ['$scope', '$http', '$q', '$state', 'localStorageService', 'scheduleFactory', 'adapter', function($scope, $http, $q, $state, localStorageService, scheduleFactory, adapter) {

    $scope.goToSchedule = function(schedule, index) {
        localStorageService.set('whichSchedule', schedule);
        scheduleFactory.setCompleteData($scope.requestDetails[index]);
        $state.go('Schedule');

    }

    $scope.truckDetails = scheduleFactory.getTruckDetails();


    function getScheduleDetails() {
        //alert("function Called");

        $scope.loader = true;
        var def = $q.defer();
        var req = {
            method: 'GET',
            //url : "js/document.json"
            url: "http://bookfair-qa.sintl.org/scholastic/mobileApps/getPendingScheduleDetails/" + $scope.truckDetails.truckId
        };
        $http(req).success(function(data) {
            $scope.loader = false;
            //app.savePushScheduleData(data,successCallback,errorCallback);
            $scope.requestDetails = data;


            for (var i = 0; i < data.length; i++) {
                var deliveryAreaList = [];

                if($scope.requestDetails[i].sequenceList!==null && $scope.requestDetails[i].sequenceList!==undefined){
                	for (var j = 0; j < $scope.requestDetails[i].sequenceList.length; j++) {
                                    deliveryAreaList.push($scope.requestDetails[i].sequenceList[j].deliveryAreaName);
                                }
					$scope.requestDetails[i].deliveryAreaList = deliveryAreaList;
                }

            }




            def.resolve(data);

        }).error(function(error) {
            $scope.loader = false;
            def.reject("Failed to get Localize String");
        });
        return def.promise;
    };


    getScheduleDetails();

    $scope.acceptSchedule = function() {
        app.savePushScheduleDataMultiple($scope.requestDetails, successCallback, errorCallback);
    }




    function successCallback() {
        //$scope.requestDetails=scheduleFactory.getCompleteData();
        //$scope.$apply();

        var scheduleListArr = [];
         $scope.loader = true;

        for (var i = 0; i < $scope.requestDetails.length; i++) {
        var bookfairIdList=[];

        if($scope.requestDetails[i].sequenceList!==null && $scope.requestDetails[i].sequenceList!==undefined){
        			for(var j=0;j<$scope.requestDetails[i].sequenceList.length;j++){
        					bookfairIdList.push($scope.requestDetails[i].sequenceList[j].sequenceId);
                	}
        }


        	var singleObj={
        		status:"Driver Accepted",
        		scheduleId:$scope.requestDetails[i].scheduleID,
        		sequenceIdList:bookfairIdList

        	}
            scheduleListArr.push(singleObj);
        }


        var url = "mobileApps/updateScheduleStatusMob";

        adapter.getServiceData(scheduleListArr, url).then(success, error);


        function success(result) {
            //alert(JSON.stringify(result));
             $scope.loader = false;
             //$scope.$apply();
            $state.reload();

        }

        function error(message) {
        	$scope.loader = false;
           // $scope.$apply();
            console.log(message);
        }



    }

    function errorCallback(message) {
        alert(message);
    }



}]);