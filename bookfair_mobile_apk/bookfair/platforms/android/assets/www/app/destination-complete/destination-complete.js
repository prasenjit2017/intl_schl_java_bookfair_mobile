app.controller("destinationCompleteController", ['$scope', '$http', '$q', '$state', 'localStorageService', 'scheduleFactory', function($scope, $http, $q, $state, localStorageService, scheduleFactory) {

    $scope.currentIndex = localStorageService.get('currentIndex');
    $scope.totalDeliveryLength = localStorageService.get('totalDeliveryLength');

    $scope.close = function() {

        /*$scope.currentIndex=$scope.currentIndex+1;

        localStorageService.set('currentIndex',$scope.currentIndex);*/

        if ($scope.doNotShowAgain) {
            var bool = true;
            localStorageService.set('doNotShowAgain', bool);
        } else {
            var bool = false;
            localStorageService.set('doNotShowAgain', bool);
        }


        if ($scope.currentIndex < $scope.totalDeliveryLength - 1) {
            $scope.currentIndex = $scope.currentIndex + 1;

            $scope.completeData = scheduleFactory.getCompleteData();

            for (var i = $scope.currentIndex; i < $scope.totalDeliveryLength; i++) {
                if ($scope.completeData.sequenceList[i].status === 'Driver Accepted' || $scope.completeData.sequenceList[i].status === undefined || $scope.completeData.sequenceList[i].status === 'Ongoing') {
                    localStorageService.set('currentIndex', i);
                    $state.go('Destination');
                    break;
                } else {
                    if (i == $scope.totalDeliveryLength - 1) {
                        $state.go('Schedule');
                        break;
                    }
                }

            }




        } else {

            $state.go('Schedule');
        }



        //$state.go('Destination');
    }



}]);