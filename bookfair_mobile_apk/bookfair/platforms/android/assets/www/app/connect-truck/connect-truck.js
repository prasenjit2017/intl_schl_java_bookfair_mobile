app.controller("truckController", ['$scope', '$http', '$q', '$state', 'scheduleFactory', 'adapter', 'localStorageService', '$cordovaNetwork', function($scope, $http, $q, $state, scheduleFactory, adapter, localStorageService, $cordovaNetwork) {

    $scope.liscensePlate = "";
    var isOffline = 'onLine' in navigator && !navigator.onLine;
    $scope.offline = isOffline;
    $scope.sessionBlock=false;
    $scope.wrongLicense=false;

    if (!isOffline) {
        app.fetchAllData(successCall, errorCall);
    }


    function successCall(data) {

        if (data !== null && data !== undefined && data.length > 0) {
            var url = "mobileApps/syncDataFromMob";

            var options = {
                dimBackground: true
            };

            SpinnerPlugin.activityStart("Syncing data", options);


            adapter.getServiceData(data, url).then(success, error);

            function success(result) {
                SpinnerPlugin.activityStop();
                //console.log(result);
                if (result !== null && result !== undefined && result.length > 0) {
                    app.deleteAllData(result);
                }

            }

            function error(error) {
                SpinnerPlugin.activityStop();
                console.log(error);
            }
        }

    }

    function errorCall(error) {

    }

    $scope.submitLiscensePlate = function() {

    if($scope.liscensePlate!==null && $scope.liscensePlate!==undefined && $scope.liscensePlate!==""){
    	var reqData = {};
                var url = "mobileApps/getTruckDetailsInMob/" + $scope.liscensePlate;

                adapter.getServiceDataGet(reqData, url).then(success, error);


                function success(result) {
                    //alert(JSON.stringify(result));

                    if (result.truckName !== null && result.isActiveFlag!=="Yes") {
                        scheduleFactory.setTruckDetails(result);
                        localStorageService.set('truckDetails', result);
                        $scope.sessionBlock=false;
                        $scope.wrongLicense=false;
                        $scope.sessionBlock=false;

                        updateTruckActiveFlag();

                       // $state.go('MobileDashboard');
                    }else if(result.isActiveFlag==="Yes"){
                    	$scope.sessionBlock=true;
                    	$scope.offline=false;
                    	$scope.wrongLicense=false;


                    } else {
                        scheduleFactory.setTruckDetails(null);
                        localStorageService.set('truckDetails', null);
                        $scope.wrongLicense=true;
                        $scope.sessionBlock=false;
                    }

                }

                function error(message) {
                    console.log(message);
                    scheduleFactory.setTruckDetails(null);
                    localStorageService.set('truckDetails', null);
                    $scope.offline=true;
                    $scope.sessionBlock=false;
                    $scope.wrongLicense=false;
                }
    }



        //	scheduleFactory.setTruckDetails(truck);
        //
    }


    function updateTruckActiveFlag(){
    	var reqData = {};
                var url = "mobileApps/updateTruckIsActiveFlag";

                reqData={
                	license:$scope.liscensePlate,
                	isActiveFlag:"Yes"
                }

                adapter.getServiceData(reqData, url).then(success, error);


                function success(result) {
                    //alert(JSON.stringify(result));
                        $state.go('MobileDashboard');


                }

                function error(message) {
                    console.log(message);

                }
    }


    $scope.$on('$cordovaNetwork:online', function(event, networkState) {
        $scope.offline = false;

    });

    $scope.$on('$cordovaNetwork:offline', function(event, networkState) {
        $scope.offline = true;

    });

}]);