app.controller("destinationController", ['$scope', '$http', '$state', 'scheduleFactory', 'localStorageService','adapter', function($scope, $http, $state, scheduleFactory, localStorageService,adapter) {

    $scope.showReservationDetails = true;
    $scope.showReservedItems = false;
    $scope.showShippingAddress = false;
    $scope.showContactDetails = false;
    $scope.showImageDetails = false;

    $scope.loader = true;

    $scope.whichSchedule = localStorageService.get('whichSchedule');

    //	//Remove later
    //	$scope.whichSchedule="current";

    $scope.currentIndex = localStorageService.get('currentIndex');
    $scope.totalDeliveryLength = localStorageService.get('totalDeliveryLength');

    $scope.completeData = scheduleFactory.getCompleteData();

    $scope.destinationData = $scope.completeData.sequenceList[$scope.currentIndex];

     var monthNames=["January","February","March","April","June","July","August","September","October","November","December"];


    if ($scope.destinationData.delivery === "Delivery") {
        var currentDate = new Date();
        currentDate=currentDate.setHours(0, 0, 0, 0);
        var deliveryDate = new Date($scope.destinationData.departureDate);
        var deliveryDate1=new Date($scope.destinationData.departureDate);
        deliveryDate=deliveryDate.setHours(0, 0, 0, 0);



        $scope.departureDate=deliveryDate1.getDate()+" "+monthNames[deliveryDate1.getMonth()]+" "+deliveryDate1.getFullYear();

        if (currentDate === deliveryDate) {
            $scope.whichSchedule = "current";
        } else if (currentDate > deliveryDate) {
            $scope.whichSchedule = "past";
        } else {
            $scope.whichSchedule = "upcoming";
        }
    } else {
        var currentDate = new Date();
       currentDate= currentDate.setHours(0, 0, 0, 0);
        var deliveryDate = new Date($scope.destinationData.returnDate);
        var deliveryDate1 = new Date($scope.destinationData.returnDate);
        deliveryDate=deliveryDate.setHours(0, 0, 0, 0);

        if (currentDate === deliveryDate) {
            $scope.whichSchedule = "current";
        } else if (currentDate > deliveryDate) {
            $scope.whichSchedule = "past";
        } else {
            $scope.whichSchedule = "upcoming";
        }
        $scope.returnDate=deliveryDate1.getDate()+" "+monthNames[deliveryDate1.getMonth()]+" "+deliveryDate1.getFullYear();
    }

    $scope.truckDetails = scheduleFactory.getTruckDetails();




    $scope.recordToSave = {
        trsxId: $scope.destinationData.trsxId,
        productDetails: $scope.destinationData.itemDetails
    };
    localStorageService.set('recordToSave', $scope.recordToSave);

    $scope.loader = false;

    $scope.tabClick = function(tabName) {

        if (tabName === 'reservationDetails') {
            $scope.showReservationDetails = true;
            $scope.showReservedItems = false;
            $scope.showShippingAddress = false;
            $scope.showContactDetails = false;
            $scope.showImageDetails = false;
            //$(window).scrollTop($('#reservationDetails').offset().top);
            $('html,body').animate({
                    scrollTop: $("#reservationDetails").offset().top
                },
                'slow');

        } else if (tabName === 'reservedItems') {
            $scope.showReservationDetails = false;
            $scope.showReservedItems = true;
            $scope.showShippingAddress = false;
            $scope.showContactDetails = false;
            $scope.showImageDetails = false;
            //$(window).scrollTop($('#itemDetails').offset().top);
            $('html,body').animate({
                    scrollTop: $("#itemDetails").offset().top
                },
                'slow');

        } else if (tabName === 'shippingAddress') {
            $scope.showReservationDetails = false;
            $scope.showReservedItems = false;
            $scope.showShippingAddress = true;
            $scope.showContactDetails = false;
            $scope.showImageDetails = false;
            //$(window).scrollTop($('#shippingDetails').offset().top);
            $('html,body').animate({
                    scrollTop: $("#shippingDetails").offset().top
                },
                'slow');

        } else if (tabName === 'contactDetails') {
            $scope.showReservationDetails = false;
            $scope.showReservedItems = false;
            $scope.showShippingAddress = false;
            $scope.showContactDetails = true;
            $scope.showImageDetails = false;
            //$(window).scrollTop($('#contactDetails').offset().top);
            $('html,body').animate({
                    scrollTop: $("#contactDetails").offset().top
                },
                'slow');

        } else if (tabName === 'imageDetails') {
            $scope.showReservationDetails = false;
            $scope.showReservedItems = false;
            $scope.showShippingAddress = false;
            $scope.showContactDetails = false;
            $scope.showImageDetails = true;
            //$(window).scrollTop($('#imageDetails').offset().top);
            $('html,body').animate({
                    scrollTop: $("#imageDetails").offset().top
                },
                'slow');
        }

    }

    $scope.endDestination = function() {
        $state.go('DeliveryChecklist');

    }

    $scope.startDestination = function() {
        $scope.destinationData.status = "Ongoing";
        $scope.completeData.sequenceList[$scope.currentIndex] = $scope.destinationData;
		scheduleFactory.setCompleteData($scope.completeData);

        if (localStorageService.get('whichSchedule') === 'current') {
            app.fetchScheduleData($scope.truckDetails.license, successCallback, errorCallback);
        }



    }

    function successCallback(data) {
        $scope.userData = data;
        $scope.userData = $scope.completeData;

        $scope.$apply();

        var singleObj={
        status:"Ongoing",
        scheduleId:$scope.userData.scheduleID,
        sequenceIdList:[$scope.destinationData.sequenceId]
        }

        var scheduleListArr=[];

        scheduleListArr.push(singleObj);

        var url = "mobileApps/updateScheduleStatusMob";


        adapter.getServiceData(scheduleListArr, url).then(success, error);


                function success(result) {
                    app.updateScheduleData($scope.userData);

                }

                function error(message) {
                	//$scope.loader = false;
                   // $scope.$apply();
                    console.log(message);
                    app.updateScheduleData($scope.userData);
                }



    }

    function errorCallback(message) {

    }


    $scope.goBack=function(){
    	localStorageService.set('whichSchedule',"");
    	$state.go('Schedule');

    }

    //imageDetails

    $(document).on('scroll', function() {

        if ($state.current.name === 'Destination') {
            if ($(this).scrollTop() <= $('#reservationDetails').position().top) {

                $scope.showReservationDetails = true;
                $scope.showReservedItems = false;
                $scope.showShippingAddress = false;
                $scope.showContactDetails = false;
                $scope.showImageDetails = false;
                $scope.$apply();

            } else if ($(this).scrollTop() <= $('#itemDetails').position().top) {

                $scope.showReservationDetails = false;
                $scope.showReservedItems = true;
                $scope.showShippingAddress = false;
                $scope.showContactDetails = false;
                $scope.showImageDetails = false;
                $scope.$apply();

            } else if ($(this).scrollTop() <= $('#shippingDetails').position().top) {

                $scope.showReservationDetails = false;
                $scope.showReservedItems = false;
                $scope.showShippingAddress = true;
                $scope.showContactDetails = false;
                $scope.showImageDetails = false;
                $scope.$apply();

            } else if ($(this).scrollTop() <= $('#contactDetails').position().top) {
                $scope.showReservationDetails = false;
                $scope.showReservedItems = false;
                $scope.showShippingAddress = false;
                $scope.showContactDetails = true;
                $scope.showImageDetails = false;
                $scope.$apply();


            } else {
                $scope.showReservationDetails = false;
                $scope.showReservedItems = false;
                $scope.showShippingAddress = false;
                $scope.showContactDetails = false;
                $scope.showImageDetails = true;
                $scope.$apply();

            }
        }


    })
}]);