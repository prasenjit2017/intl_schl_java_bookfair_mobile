app.controller("scheduleController", ['$scope', '$http', '$state', 'scheduleFactory', 'localStorageService','adapter', function($scope, $http, $state, scheduleFactory, localStorageService,adapter) {

    $scope.showAccepted = false;
    $scope.showScheduleId = false;
    $scope.showBlank = false;
    $scope.loader = true;

    $scope.truckDetails = scheduleFactory.getTruckDetails();


    var isOffline = 'onLine' in navigator && !navigator.onLine;
    $scope.offline = isOffline;

    $scope.$on('$cordovaNetwork:online', function(event, networkState) {
        $scope.offline = false;
    });

    $scope.$on('$cordovaNetwork:offline', function(event, networkState) {
        $scope.offline = true;
    });


    $scope.goBack = function() {
        if (!$scope.offline) {
            $state.go('MobileDashboard');
        }
    }

    $scope.whichSchedule = localStorageService.get('whichSchedule');

    if (localStorageService.get('whichSchedule') === 'current') {
        app.fetchScheduleData($scope.truckDetails.license, successCallback, errorCallback);

    } else {
        $scope.userData = {};
        $scope.userData = scheduleFactory.getCompleteData();

        if ($scope.userData !== undefined && $scope.userData.scheduleID !== undefined) {
            $scope.showBlank = false;
            $scope.loader = false;
            $scope.todayDate = new Date();
            //alert($scope.todayDate.toString());

            //        	var monthNames = [
            //                "January", "February", "March",
            //                "April", "May", "June", "July",
            //                "August", "September", "October",
            //                "November", "December"
            //              ];

            var month = $scope.todayDate.getMonth() + 1;
            if (month < 10) {
                month = "0" + month;
            }

            var tarikh=$scope.todayDate.getDate();

            if(tarikh<10){
            	tarikh="0"+tarikh;
            }

            $scope.todayDate=$scope.todayDate.getFullYear()+"-"+month+"-"+tarikh;
           // $scope.todayDate = $scope.todayDate.getDate() + "-" + month + "-" + $scope.todayDate.getFullYear();

            //alert($scope.todayDate);

            var dateRange = $scope.userData.scheduleRange;

            if (dateRange.toLowerCase() !== "na") {

                dateRange = dateRange.replace(/\s/g, "");
                var range=dateRange.split("to");

                var firstDate = new Date(range[0]);
                var secondDate = new Date(range[1]);

                firstDate=firstDate.setHours(0, 0, 0, 0);
                secondDate=secondDate.setHours(0, 0, 0, 0);
                var currentDate = new Date();
                currentDate=currentDate.setHours(0, 0, 0, 0);

                if (firstDate <= currentDate && secondDate >= currentDate) {
                    $scope.whichSchedule = 'current';
                    localStorageService.set('whichSchedule', $scope.whichSchedule);
                }else if(secondDate<currentDate){
                	$scope.whichSchedule = 'past';
					localStorageService.set('whichSchedule', $scope.whichSchedule);
                }else if(firstDate>currentDate){
                	$scope.whichSchedule = 'upcoming';
					localStorageService.set('whichSchedule', $scope.whichSchedule);
                }

            } else {
                if ($scope.userData.date!==null && $scope.userData.date!==undefined && $scope.todayDate === $scope.userData.date) {
                    $scope.whichSchedule = 'current';
                    localStorageService.set('whichSchedule', $scope.whichSchedule);
                }
            }

            $scope.showButton=true;

			for(var i=0;i<$scope.userData.sequenceList.length;i++){
				if($scope.userData.sequenceList[i].status!=="Driver Accepted"){
					$scope.showButton=false;
				}
			}


        } else {
            $scope.showBlank = true;
            $scope.loader = false;
        }




    }

    var swipeCount = 0;




    function successCallback(data) {

        //alert(JSON.stringify(data));

        $scope.userData = data;

        if ($scope.userData !== undefined && $scope.userData.scheduleID !== undefined) {
            $scope.showBlank = false;
            $scope.loader = false;
            scheduleFactory.setCompleteData(data);

            if ($scope.userData.status === 'Finalized') {
                $scope.btnText = "Swipe right to accept";
            } else {
                $scope.btnText = "Swipe right to start";
            }
            $scope.showButton=true;

			for(var i=0;i<$scope.userData.sequenceList.length;i++){
				if($scope.userData.sequenceList[i].status!==null || $scope.userData.sequenceList[i].status!=="Driver Accepted"){
					$scope.showButton=false;
				}
			}


            $scope.$apply();
        } else {
            $scope.showBlank = true;
            $scope.loader = false;
            $scope.$apply();
        }

    }

    function errorCallback(message) {
        //alert(message);
        $scope.userData = {};
        $scope.showBlank = true;
        $scope.loader = false;
        $scope.$apply();
    }

    function errorCallbackFetch(message) {
        $scope.loader = false;
    }


    function successCallbackFetch(data) {


    }

    /*function errorCallback(message) {
    	$scope.loader=false;
        console.log(message);
    }*/


    $scope.endSchedule = function() {

        var count = 0;
        for (var i = 0; i < $scope.userData.sequenceList.length; i++) {
            if ($scope.userData.sequenceList[i].status === 'Completed') {
                count++;
            }
        }

        if (count === $scope.userData.sequenceList.length) {
            $scope.userData.status = "Completed";

            var sequenceIdList=[];
            for(var i=0;i<$scope.userData.sequenceList.length;i++){
            	sequenceIdList.push($scope.userData.sequenceList[i].sequenceId);
            }

            var singleObj={
            	status:"Completed",
            	scheduleId:$scope.userData.scheduleID,
            	sequenceIdList:sequenceIdList
            }

            var scheduleListArr=[];

                    scheduleListArr.push(singleObj);

                    var url = "mobileApps/updateScheduleStatusMob";


                    adapter.getServiceData(scheduleListArr, url).then(success, error);


                            function success(result) {
                                if ($scope.whichSchedule === 'current')
                                                app.updateScheduleData($scope.userData);

                            }

                            function error(message) {
                            	//$scope.loader = false;
                               // $scope.$apply();
                                console.log(message);
                                if ($scope.whichSchedule === 'current')
                                                app.updateScheduleData($scope.userData);
                            }


        }

    }

    $scope.acceptSchedule = function() {
        //alert("You swiped right");
        swipeCount++;
        if ($scope.userData.status === 'Finalized') {

        $scope.loader=true;


        var bookfairIdList=[];

        if($scope.userData.sequenceList!==null && $scope.userData.sequenceList!==undefined){
        	for(var i=0;i<$scope.userData.sequenceList.length;i++){
                            	bookfairIdList.push($scope.userData.sequenceList[i].sequenceId);
			}
        }



                var singleObj={
                	status:"Driver Accepted",
                	scheduleId:$scope.userData.scheduleID,
                	sequenceIdList:bookfairIdList
                }

                var reqData=[];
                reqData.push(singleObj);
                var url = "mobileApps/updateScheduleStatusMob";

                adapter.getServiceData(reqData, url).then(success, error);


                function success(result) {
                    //alert(JSON.stringify(result));
                    $scope.loader=false;
        			$scope.showAccepted = true;
					$(".schedule-accepted-box").fadeTo(2000, 500).slideUp(2000);


					$scope.btnText = "Swipe right to start";
					$scope.userData.status = "Driver Accepted";

					if($scope.userData.sequenceList!==null && $scope.userData.sequenceList!==undefined){
						for(var i=0;i<$scope.userData.sequenceList.length;i++){
							$scope.userData.sequenceList[i].status="Driver Accepted";
						}
					}

					scheduleFactory.setCompleteData($scope.userData);

					app.savePushScheduleData($scope.userData, successCallbackFetch, errorCallbackFetch);


					$scope.showScheduleId = true;
                }

                function error(message) {
                $scope.loader=false;
                    console.log(message);
                }



        } else if ($scope.userData.status === "Driver Accepted") {
            /*scheduleFactory.setScheduleData($scope.userData.scheduleDetails.deliveries);

            scheduleFactory.updateDeliveryStatus(0,"Ongoing");*/

            //   Uncomment later if block
            if ($scope.whichSchedule === 'current') {
                //app.updateScheduleData($scope.userData);
                // $scope.userData.sequenceList[0].status = "Ongoing";
               // $scope.userData.status = "Started";
                scheduleFactory.setCompleteData($scope.userData);
                localStorageService.set('currentIndex', 0);
                localStorageService.set('totalDeliveryLength', $scope.userData.sequenceList.length);
                app.updateScheduleData($scope.userData);
                $state.go('Destination');
            }

        }



    }


    $scope.startDelivery = function(index) {
        //scheduleFactory.setScheduleData($scope.userData.deliveries);


        /*if ($scope.userData.status === 'Driver Accepted' && $scope.whichSchedule === 'current') {
            $scope.userData.status = "Started";
        }*/



        localStorageService.set('currentIndex', index);
        localStorageService.set('totalDeliveryLength', $scope.userData.sequenceList.length);
        app.updateScheduleData($scope.userData);
        $state.go('Destination');
    }


}]);